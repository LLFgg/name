var faker = require('faker');
var _ = require('lodash');

module.exports = function() {
  var data = {
    categories: [],
    posts: [],
  };

  // Create categories
  for (var i = 1; i <= 3; i++) {
    data.categories.push({
      id: i,
      name: _.capitalize(faker.lorem.word())
    });
  }

  // Create posts
  for (var i = 1; i <= 5; i++) {
    data.posts.push({
      id: i,
      category_id: _.shuffle(data.categories)[0].id,
      title: `用户${i}`,
      body: "年龄：10\n性别：男\n姓名：liu\n头像：http://example.com" 
    });
  }

  return data;
}();
