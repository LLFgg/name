//数据校验
var Joi = require('joi');

var categoryBody = {
  name: Joi.string().min(2).required()
};

var postBody = {
  title: Joi.string().min(0).max(80).required(),
  body: Joi.string().min(0).max(100).required()
};

module.exports = {
  category: {
    body: categoryBody
  },
  post: {
    body: postBody
  },
};
