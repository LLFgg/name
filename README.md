## 说明

环境：windows  
使用json-server和faker模块来作为api数据源，快速构造restful API  
前端使用了immutable以及redux-observable  

## 使用

1.git clone https://LLFgg@bitbucket.org/LLFgg/name.git   
2.cd name  
3.cnpm install(推荐使用cnpm安装)  
4.cd server  
5.cnpm install  
6.cd ..   
7.npm run server-api(开启服务器)  
8.npm start(打包)  
9.打开 http://localhost:8000  